# Implements arrays of ragged columns.

# TODO: subtyping of abstract array does not work with show!

module Ragged

importall Base #: +,-,/,*,show,getindex,setindex!

export RaggedArray

## End-type
# =========
# A type which accumulates operations done to it, which can then
# executed later to get around the end-indexing problem with ragged arrays.
# It only supports +,-,*,/
immutable End
    terms::((Function, Real)...,)
end
End() = End(())
show(io::IO, en::End) = print(io, "ragged")  # we're using is for ragged arrays
+(en::End) = en
+(en::End, ii::Real) = End(tuple(en.terms..., (+,ii)))
+(ii::Real, en::End) = en + ii
-(en::End) = End(tuple(en.terms..., (*,-1)))
-(en::End, ii::Real) = End(tuple(en.terms..., (-,ii)))
-(ii::Real, en::End) = -(en - ii)
*(en::End, ii::Real) = End(tuple(en.terms..., (*,ii)))
*(ii::Real, en::End) = en * ii
/(en::End, ii::Real) = End(tuple(en.terms..., (/,ii)))
div(en::End, ii::Int) = End(tuple(en.terms..., (div,ii)))
isequal(en1::End, en2::End) = isequal(en1.terms, en2.terms)
function evalEnd(en::End, endsize)
    for (f,i) in en.terms
        endsize = f(endsize,i)
    end
    endsize
end
# ID-function if something other than End gets passed in:
evalEnd(en, endsize) = en 

# ranges based on End
abstract EndRange{T, S<:Int} <: OrdinalRange{T, S}
first(ser::EndRange) = ser.start
last(ser::EndRange) = ser.stop
# checks that it is 1:end
is_full_range(enr::EndRange) = ( first(enr)==1 && step(enr)==1 && isa(enr.stop, End) && length(enr.stop.terms)==0 )

immutable StepEndRange{T,S} <: EndRange{T,S}
    start::Union(T, End)
    step::S
    stop::Union(T, End)
end
step(ser::StepEndRange) = ser.step
length(r::StepEndRange) = div(r.stop+r.step - r.start, r.step)

colon(start::End, step, stop::End) = StepEndRange(start, step, stop)
colon(start, step, stop::End) = StepEndRange(start, step, stop)
colon{T,S}(start::End, step::S, stop::T) = StepEndRange{T,S}(start, step, stop)

evalEndRange(enr::StepEndRange, endsize) = evalEnd(enr.start, endsize) : enr.step : evalEnd(enr.stop, endsize)

immutable UnitEndRange{T} <: EndRange{T, Int}
    start::Union(T, End)
    stop::Union(T, End)
end
step(ser::UnitEndRange) = 1
length(ser::UnitEndRange) = ser.stop - ser.start + 1
show(io::IO, r::UnitEndRange) = print(io, repr(first(r)), ':', repr(last(r)))

colon(start::End, stop::End) = UnitEndRange(start, stop)
colon(start, stop::End) = UnitEndRange(start, stop)
colon{T}(start::End, stop::T) = UnitEndRange{T}(start, stop)

evalEndRange(enr::UnitEndRange, endsize) = evalEnd(enr.start, endsize) : evalEnd(enr.stop, endsize)


## Ragged Arrays
################
abstract AbstractRaggedArray{Tv} <: AbstractMatrix{Tv} # subtyping AbstractMatrix make show misbehave
eltype{Tv}(::AbstractRaggedArray{Tv}) = Tv # needed because subtyping does not work

# bounds checks (not the fastest but probably plenty fast)
function check_bounds(ra::AbstractRaggedArray, col::Integer)
    # checks that column exists
    if !(1<=col<=size(ra,2))
        error(BoundsError)
    end
end
function check_bounds(ra::AbstractRaggedArray, cols::AbstractVector)
    if !isempty(cols)
        check_bounds(ra, minimum(cols))
        check_bounds(ra, maximum(cols))
    end
end
function check_bounds(ra::AbstractRaggedArray, i0::Integer, i1::Integer)
    # checks two indices
    cl = collength(ra, i1)
    cols = size(ra,2)
    if !(1<=i1<=cols && 1<=i0<=cl)
        error(BoundsError)
    end
end
function check_bounds(ra::AbstractRaggedArray, i0::AbstractVector, i1::Integer)
    # checks indices into a particular column
    if !isempty(i0)
        check_bounds(ra, minimum(i0), i1)
        check_bounds(ra, maximum(i0), i1)
    end
end

function show(io::IO, ra::AbstractRaggedArray)
    # prints one column
    print_col(col) = isempty(col) ? println("[]") : (Base.showarray(io::IO, col', header=false), println(""))
    cols = size(ra,2)
    println(io, "Showing (transpose of) Ragged Array with ", cols, " columns and ",
            length(ra.val), " entries of type ", eltype(ra),":")
    if cols==0
        println("empty")
    elseif cols<20
        for col in ra
            print_col(col)
        end
    else
        for ii=1:10
            print_col(ra[:,ii])
        end
        println("...")
        for ii=cols-10:cols
            print_col(ra[:,ii])
        end
    end
end

## Show for arrays is a big mess:
## https://github.com/JuliaLang/julia/issues/6117 
# Overloading Base.showarray does not work for some reason
# Base.showarray(io::IO, ra::AbstractRaggedArray,
#                    header::Bool=true, limit::Bool=_limit_output,
#                    rows = Base.tty_rows()-4, cols = Base.tty_cols(), repr=false) = show(io, ra)
# Base.showarray(ra::AbstractRaggedArray; kw...) = Base.showarray(STDOUT, ra; kw...)
function display(d::Base.REPL.REPLDisplay, ::MIME"text/plain", ra::AbstractRaggedArray)
    io = Base.REPL.outstream(d.repl)
    show(io, ra)
    println(io)
end
display(d::Base.REPL.REPLDisplay, ra::AbstractRaggedArray) = display(d, MIME("text/plain"), ra)

# For ragged arrays it does not make sense to index columns ra[i,:]
# (or it would need a lot of bounds checking!)
getindex(ra::AbstractRaggedArray, i0, i1::AbstractVector ) = 
                 error("Slicing for rows does not make much sense for RaggedArrays")

# Iterator methods to iterate over the columns, state is the column
# index.
start(ra::AbstractRaggedArray) = 1
done(ra::AbstractRaggedArray, state) = state==size(ra,2)+1

ndims(ra::AbstractRaggedArray) = 2 # always 2D

# INTERFACE to RaggedArray should be as for AbstractMatrix with the following additions:
collength(ra::AbstractRaggedArray, col::Integer) = error("Provide collength: length of a particular column.")
next(ra::AbstractRaggedArray, state) = error("Provide next for concrete implementations of AbstractRaggedArray.")
size(ra::AbstractRaggedArray) = error("""Provide size for concrete implementations of AbstractRaggedArray, 
                                         returns End as first element.""")

getindex(ra::AbstractRaggedArray, i0::Integer) = error("not implemented")
getindex(ra::AbstractRaggedArray, i0::AbstractVector) = error("not implemented")
getindex(ra::AbstractRaggedArray, i0, i1::Integer) = error("not implemented") 
getindex{Tv}(ra::AbstractRaggedArray{Tv}, i0::EndRange, i1::AbstractVector) = error("not implemented") 

setindex!(ra::AbstractRaggedArray, newval, i0::Integer) = error("not implemented") 
setindex!(ra::AbstractRaggedArray, newvals, i0::AbstractVector) = error("not implemented") 
setindex!(ra::AbstractRaggedArray, newvals, i0, i1) = error("not implemented") 


## Concrete realisation
#######################
type RaggedArray{Tv, Ti<:Integer} <: AbstractRaggedArray{Tv}
    # Array of ragged colums: A way to store vectors of dissimilar
    # length in a single array, i.e. an implementation of a ragged
    # array.  It uses a similar principal as the sparse matrix
    # compressed row/column format but needs one less level of
    # indirection.
    colptr::Vector{Ti}      # Column i is in val[colptr[i]:(colptr[i+1]-1)]
    val::Vector{Tv}       # Nonzero values
end
function RaggedArray{Tv}(vecvec::Vector{Vector{Tv}})
    # construct from vectors nested in a vector
    vallen = 0
    for subv in vecvec
        vallen += length(subv)
    end
    colptr = ones(Int, length(vecvec)+1)
    val = zeros(Tv, vallen)
    for (ii,subv) in enumerate(vecvec)
        colptr[ii+1] = length(subv) + colptr[ii]
        val[colptr[ii]:(colptr[ii+1]-1)] = subv
    end
    RaggedArray(colptr, val)
end
RaggedArray(Tv, nvals::Integer, ncols::Integer) =  RaggedArray(ones(Int, ncols+1), Array(Tv, nvals))
RaggedArray(Tv, Ti, nvals::Integer, ncols::Integer) = RaggedArray(ones(Ti, ncols+1), Array(Tv, nvals))
RaggedArray{I<:Integer}(Tv::DataType, colwidths::Vector{I}) = (colptr=[1,1.+cumsum(colwidths)]; 
                                                               RaggedArray(colptr, Array(Tv, colptr[end]-1)))

size(ra::RaggedArray) = (End(), length(ra.colptr)-1)
size(ra::RaggedArray, i) = size(ra)[i]
length(ra::RaggedArray) = ra.colptr[end]-1
endof(ra::RaggedArray) = length(ra::RaggedArray)
collength(ra::RaggedArray, col::Integer) = ra.colptr[col+1] - ra.colptr[col]

next(ra::RaggedArray, state) = (ra.val[ra.colptr[state]:(ra.colptr[state+1]-1)], state+1)

maximum(ra::RaggedArray) = maximum(ra.val)
minimum(ra::RaggedArray) = minimum(ra.val)

## maths
.*(ra::RaggedArray, other::Number) = RaggedArray(ra.colptr, ra.val.*other)
.*(other::Number, ra::RaggedArray) = ra.*other
# *(ra::RaggedArray, other::Number)  = RaggedArray(ra.colptr, ra.val.*other)
# *(other::Number, ra::RaggedArray)  = ra*other
# /(ra::RaggedArray, other::Number)  = RaggedArray(ra.colptr, ra.val/other)
./(ra::RaggedArray, other::Number) = RaggedArray(ra.colptr, ra.val./other)


## equality
.==(ra1::RaggedArray, ra2::RaggedArray) = ra1.val.==ra2.val
==(ra1::RaggedArray, ra2::RaggedArray) = ra1.val==ra2.val

## getindex & setindex
#---------------------
getindex(ra::RaggedArray, i0::Integer) = ra.val[i0]
getindex(ra::RaggedArray, i0::AbstractVector) = ra.val[i0]
function getindex{Tv, Ti}(ra::RaggedArray{Tv, Ti}, i0::EndRange, i1::AbstractVector)
    check_bounds(ra, i1)
    is_full_range(i0) ? nothing : error(BoundsError) # only allow 1:end as first index
    vals = Tv[]
    colptr = Ti[1]
    for ind in i1
        nind = ra_sub2ind(ra, i0, ind)
        append!(vals, ra.val[nind])
        push!(colptr, colptr[end]+length(nind))
    end
    return RaggedArray{Tv, Ti}(colptr, vals)
end
getindex(ra::RaggedArray, i0, i1::Integer) = ra.val[ra_sub2ind(ra, i0, i1)]

setindex!(ra::RaggedArray, newval, i0::Integer) = ra.val[i0] = newval
setindex!(ra::RaggedArray, newvals, i0::AbstractVector) = ra.val[i0] = newvals
setindex!(ra::RaggedArray, newvals, i0, i1) = ra.val[ra_sub2ind(ra, i0, i1)] = newvals
# function setindex!{Tv, Ti}(ra::RaggedArray{Tv, Ti}, i0::EndRange, i1::AbstractVector)
#     error("not implemented")
# end

linear_index(ra::RaggedArray, i0::Integer, i1::Integer) = ra.colptr[i1]+i0-1
col_start(ra::RaggedArray, i1::Integer) = ra.colptr[i1]-1

# The helper methods ra_sub2ind are similar to sub2ind but with
# bounds checking and working with AbstractVector. Thus it can be
# used for getindex and setindex!
function ra_sub2ind(ra::RaggedArray, i0::Integer, i1::Integer)
    check_bounds(ra, i0, i1)
    return linear_index(ra, i0, i1)
end
ra_sub2ind(ra::RaggedArray, i0::End, i1::Integer) = ra_sub2ind(ra, evalEnd(i0, collength(ra, i1)), i1)

function ra_sub2ind(ra::RaggedArray, i0::AbstractVector, i1::Integer)
    check_bounds(ra, i0, i1)
    return i0 .+ col_start(ra, i1)
end
function ra_sub2ind(ra::RaggedArray, i0::EndRange, i1::Integer)
    # allows to return empty column when i0==1:end
    check_bounds(ra, i1)
    i00 = evalEndRange(i0, collength(ra, i1))
    if isempty(i00) && is_full_range(i0)
        return []
    else
        return ra_sub2ind(ra, i00, i1)
    end
end

end #module
