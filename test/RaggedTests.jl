print("Running tests for ragged_array.jl...")

## ragged array tests
using Ragged
RG=Ragged

## End type tests
en = RG.End()
@test RG.evalEnd((en+1)*7/5-11, 7.3) == (7.3+1)*7/5-11
@test RG.evalEnd(1-en,5) == 1-5
@test RG.evalEnd(en, 7.3) == 7.3
@test RG.evalEnd(en, 6.3) == 6.3
@test en + 6.3== en + 6.3

## EndRange type tests
@test RG.evalEndRange(1:en+1, 5) == 1:6
@test RG.evalEndRange(1:en+1, -10) == 1:-10
@test RG.evalEndRange(en:en+3, 5) == 5:5+3
@test RG.evalEnd(length(-7:en+3), 5) == length(-7:5+3)

@test RG.evalEndRange(en:2:en+30, 5) == 5:2:5+30
@test RG.evalEndRange(en:-3:en-30, 5) == 5:-3:5-30
@test RG.evalEndRange(en:2:30, 5) == 5:2:30
@test RG.evalEnd(length(en:2:30), 5) == length(5:2:30)
@test RG.evalEnd(length(en:-2:-30), 5) == length(5:-2:-30)

er = en:10
@test first(er)==er.start
@test last(er)==er.stop
@test step(er)==1
@test length(er)==10-en+1
er = en:2:10
@test first(er)==er.start
@test last(er)==er.stop
@test step(er)==er.step
@test RG.evalEnd(length(er), -5)==RG.evalEnd(div(10-en+2,2), -5)

## Ragged array tests
function ret_ra(opt="default")
    if opt=="default"
        rnd = Int[ 9,8,10,8,2,4,9,6,0,3]
        colptr = [1, cumsum(rnd)]
        val = [1:colptr[end]-1]
        ra = RaggedArray(colptr, val) 
    elseif opt=="one col" 
        colptr = [1,11]
        val = [1:colptr[end]-1]
        ra = RaggedArray(colptr, val) 
    elseif opt=="empty" 
        colptr = [1]
        val = []
        ra = RaggedArray(colptr, val) 
    end
end

subinds = 3:5
ras = {ret_ra(), ret_ra("one col"), ret_ra("empty")}
ra = ret_ra()


## empty initialisation
cols=5; nvals = 17
raI = RaggedArray(Int, nvals, cols)
raIF = RaggedArray(Float64, Int, nvals, cols)
colwidths = [3,4,2]
raCW = RaggedArray(Float64, colwidths)
@test size(raI)==(RG.End(),5)
@test size(raIF)==(RG.End(),5)
@test size(raCW)==(RG.End(),length(colwidths))
@test size(ra[:,[]])==(RG.End(),0)
@test typeof(raI)==RaggedArray{Int, Int}
@test typeof(raIF)==RaggedArray{Float64, Int}
@test ndims(ra)==2
@test endof(ra)==length(ra)

## construction from nested vector
@test all(ra.val.==1:length(ra))
vecvec = Vector{Int}[[ 26, 27, 28], [], [ 12, 13, 19, 11, 26], [  4, 28, 6, 10, 20], [ 5, 11, 27, 4]]
rav = RaggedArray(vecvec)
@test typeof(rav)==RaggedArray{Int, Int}
for (ii,col) in enumerate(rav)
    @test all(col.==vecvec[ii])
    @test RG.collength(rav,ii)==length(vecvec[ii])
end

# math
@test (rav.*5.6).val == rav.val.*5.6
@test rav.*5.6 == 5.6.*rav
@test (rav./5.6).val == rav.val./5.6
@test raI!=rav
@test rav==rav
@test all(rav.==rav)

## getindex
for ra in ras
    @test length(ra.val)==length(ra)
    @test length(ra.colptr)-1==size(ra,2)
    # iteration
    for col in ra
        @test typeof(col)==Vector{Int}
    end
    # getindex
    for jj=1:length(ra.val)    
        @test ra[jj]==jj
    end
    @test ra[1:end]==[1:length(ra.val)]
    @test_throws BoundsError ra[3:100] # out of bounds 
    @test_throws BoundsError ra[100] # out of bounds 
    @test_throws ErrorException ra[5,[3]] # ERROR: Slicing for rows does not make sense for RaggedArrays
    if length(ra)>0
        @test all(ra[3:end,1].==3:length(ra[:,1]))
        @test all(ra[3:2:end,1].==3:2:length(ra[:,1]))
        @test all(ra[:].==ra.val)
        @test all(ra[3:2:end].==3:2:length(ra))
    end
    for coli=1:size(ra,2)
        for rowi=1:length(ra[:,coli]) 
            ra[rowi,coli]
        end
    end
end
ra = ras[1]
ra1 = ras[2]
rae = ras[3]
# ra tests
@test all(ra[5:10].==5:10)
@test_throws  BoundsError ra[1,11] # out of bounds 
@test_throws  BoundsError ra[5,10] # out of bounds 
@test ra[:,10] == Int[56, 57, 58]
@test ra[1:2,10] == Int[56, 57]
@test_throws  BoundsError ra[:,11] # out of bounds 
ra_tmp = ra[:,subinds]
for (ii, col) in enumerate(ra_tmp)
    @test all(col.==ra[:,subinds[ii]])
end
@test all(ra[3:end-10].==3:(length(ra)-10))

# ra1 tests
@test all(ra1[3:end].==3:10)
@test all(ra1[3:end-5].==3:(length(ra1)-5))

# rae tests
@test_throws BoundsError rae[1]   # out of bounds 
@test_throws BoundsError rae[:,1] # out of bounds 
@test size(rae[:,:]) == (RG.End(),0) # empty again


## setindex
testval = -9999
# singel index
ra = ret_ra(); ra[5] = testval
@test ra[5]==testval
ra = ret_ra(); ra[5:10] = testval
@test all(ra[5:10].==testval)
ra = ret_ra(); ra[5:10] = testval .+ [5:10]
@test all(ra[5:10].==(testval  .+ [5:10]))
ra = ret_ra(); ra[5:7:50] = testval .+ [5:7:50]
@test all(ra[5:7:50].==(testval  .+ [5:7:50]))
# two indices
ra = ret_ra(); ra[5,8] = testval
@test ra[5,8]==testval
ra = ret_ra(); ra[3:5,4] = testval
@test all(ra[3:5,4].==testval)
ra = ret_ra(); ra[3:2:7,4] = testval
@test all(ra[3:2:7,4].==testval)
ra = ret_ra(); ra[3:2:7,4] = testval .+ [3:2:7]
@test all(ra[3:2:7,4].==testval .+ [3:2:7])
ra = ret_ra(); ra[end-1,4] = testval
@test all(ra[end-1,4].==testval)
ra = ret_ra(); ra[end-4:2:end-1,4] = testval .+ [5-4:2:5-1]
@test all(ra[end-4:2:end-1,4].==testval .+ [5-4:2:5-1])
# returning RaggedArray
ra = ret_ra(); ra2 = ra[:,[1:3]];
@test all(ra2.val.==1:length(ra2))


## bounds
ra = ret_ra(); 
@test_throws BoundsError rae[1]      # out of bounds 
@test_throws BoundsError rae[:,1]    # out of bounds 
@test_throws BoundsError ra[1,11]    # out of bounds 
@test_throws BoundsError ra[5,10]    # out of bounds 
@test_throws BoundsError ra[1:10,5]  # out of bounds
@test_throws BoundsError ra[2:end,[5]]  # not good

# misc
@test maximum(rav)==28
@test minimum(rav)==4
@test length(rav)==sum(map(length, vecvec))
@test endof(rav)==sum(map(length, vecvec))
@test all(ra.==ra)
@test ra==ra


println("all passed.")

##################################################
# missing features
##################################################

check_failing_tests = false

if check_failing_tests
    println("Checking tests for ragged_array.jl which are failing:")
end