using Base.Test

test_type = length(ARGS) == 1 ? ARGS[1] : "ALL"
 
# includes
include("../src/Ragged.jl")

# some setup
if test_type == "ALL" || test_type == "RAGGEDARRAY"
    include("RaggedTests.jl")
end
